package main

import (
	"fmt"
	"net/http"
	"sort"
	"strconv"
	"strings"
)

func main() {
	n := firstInBracket("Welcome (to) GeeksforGeeks)")
	// AnagramSTR()
	fmt.Println("BRACKET = ", n)
}

// SOAL 3 Refactor Code
func firstInBracket(str string) string {
	if len(str) > 0 {
		firstBracketFound := strings.Index(str, "(")
		runes := []rune(str)
		wordFirstBracket := string(runes[firstBracketFound:len(str)])
		closingBracket := strings.Index(wordFirstBracket, ")")
		if closingBracket >= 0 {
			runes := []rune(wordFirstBracket)
			return string(runes[1 : closingBracket-1])
		} else {
			return ""
		}
	} else {
		return ""
	}
	return ""
}

// Soal 4 Logic Test - Anagram
func AnagramSTR() {
	var dict_anagram = []string{
		"kita", "atik", "tika", "aku", "kia", "makan", "kua",
	}

	fmt.Println("Dict Anagram = ", dict_anagram)

	list := make(map[string][]string)

	for _, dict := range dict_anagram {
		str := strings.Split(dict, "")
		sort.Strings(str)
		key := strings.Join(str, "")

		list[key] = append(list[key], dict)
	}

	for _, dicts := range list {
		fmt.Println(dicts)
	}
}

// Soal 2 microservice search movie
// this example with gin framework
// and library gorm to connect database
// say we have a MySQL DB table
type (
	RequestAllMovie struct {
		Search string `json:"search"`
		Page   string `json:"page"`
	}

	Movies struct {
		Title  string `json:"title" gorm:"column:title"`
		Year   string `json:"year" gorm:"column:year"`
		ImdbID string `json:"imdbID" gorm:"column:imdbID"`
		Type   string `json:"type" gorm:"column:type"`
		Poster string `json:"poster" gorm:"column:poster"`
	}

	ResponseDataMovies struct {
		TotalResult int64    `json:"totalResults"`
		Response    string   `json:"Response"`
		Search      []Movies `json:"Search"`
	}
)

func (idb *InDB) GetAllMovie(c *gin.Context) {

	var (
		request      RequestAllMovie
		movies       []Movies
		responseData []Movies
		recordsTotal int64
	)
	if err := c.ShouldBindQuery(&request); err != nil {
		c.AbortWithStatusJSON(
			http.StatusBadRequest,
			"is required",
		)
		return
	}

	page, _ := strconv.Atoi(request.Page)

	// record total "totalResult" data
	db.Table("movies").Select("title, year, imdbID, type, poster").Count(&recordsTotal)

	// query view data moview
	query := db.Table("movies").Select("title, year, imdbID, type, poster")

	GetDatatables([]string{"title"}, page, request.Search, 10)

	query.Scan(&movies)

	for _, dataAllMovies := range movies {
		responseData = append(responseData, Movies{
			Title:  dataAllMovies.Title,
			Year:   dataAllMovies.Title,
			ImdbID: dataAllMovies.ImdbID,
			Type:   dataAllMovies.Type,
			Poster: dataAllMovies.Poster,
		})
	}

	c.JSON(
		http.StatusOK,
		ResponseDataMovies{
			TotalResult: recordsTotal,
			Response:    "True",
			Search:      responseData,
		},
	)
}

func GetDatatables(ColumnSerach []string, page int, search string, length int) {
	if search != "" {
		for _, items := range ColumnSerach {
			// status = true
			statement := fmt.Sprintf("%s like ?", items)
			db = db.Or(statement, fmt.Sprintf("%%%s%%", string(search)))
		}
	}

	if page == 0 {
		page = 1
	}

	switch {
	case length > 100:
		length = 100
	case length <= 0:
		length = 10
	}

	offset := (page - 1) * length

	indb.Offset(offset).Limit(length)
}
