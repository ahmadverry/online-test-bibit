SELECT
	u1.id,
	u1.username,
	u2.username AS parent_username 
FROM
	users u1
	LEFT JOIN ( SELECT id, username FROM users ) u2 ON u2.id = u1.parent 
ORDER BY
	u1.id;